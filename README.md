## Before run the app start fake json server by typing:

json-server --watch db.json --routes routes.json --port 3030

The toast that appears twice is not a bug. React in the development mode adds additional rerender to the app and 
some libraries are not optimized so the toast shows twice, but in the production mode this behaviour goes away.
import React from 'react';
import { useSelector } from 'react-redux';
import { TbGift, TbLego } from 'react-icons/tb';

import { StartingScreen, MinifigList, FormScreen } from '../';
import { APP_STEPS } from '../../constants';
import { selectStep } from '../../store/minifigures';

import './App.scss';

const renderView = {
  [APP_STEPS.STEP_FIRST]: <StartingScreen />,
  [APP_STEPS.STEP_SECOND]: <MinifigList />,
  [APP_STEPS.STEP_THIRD]: <FormScreen />,
}

const App = () => {
  const step = useSelector(selectStep);

  if (!step) {
    return null;
  }

  return (
    <main className="app">
      <div className="app__content">
        {renderView[step]}
      </div>
      <TbLego className="app__img-1" />
      <TbGift className="app__img-2" />
    </main>
  );
}

export default App;

import React, { FC } from 'react';

import { Loader, Modal, FigureDetails } from '../';
import { FigureType, legoMinifiguresApi } from '../../api';

import './FigureDetailsModal.scss';

type FigureDetailsModalProps = {
  isOpen: boolean;
  onClose: () => void;
  figure: FigureType;
};

const FigureDetailsModal: FC<FigureDetailsModalProps> = ({ isOpen, onClose, figure }): JSX.Element => {
    // @ts-ignore
  const { data: { results  = [] } = {} , error, isLoading} = legoMinifiguresApi.useGetFigurePartsQuery(figure.set_num);

  return <Modal hasCloseBtn={true} isOpen={isOpen} onClose={onClose}>
      <Loader isLoading={isLoading} error={error}>
        {results && <FigureDetails figure={figure} parts={results} showMore />}
      </Loader>
    </Modal>
}

export default FigureDetailsModal;

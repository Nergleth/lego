import { FetchBaseQueryError } from '@reduxjs/toolkit/query';
import { SerializedError } from '@reduxjs/toolkit';

export type LoaderPropsType = {
    isLoading: boolean;
    error: FetchBaseQueryError | SerializedError | undefined;
    children: JSX.Element;
};

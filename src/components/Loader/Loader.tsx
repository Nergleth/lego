import React, { FC } from 'react';
import { TbLoader3 } from 'react-icons/tb';

import { LoaderPropsType } from './types';

import './Loader.scss';

const Loader: FC<LoaderPropsType> = ({ isLoading, error, children}): JSX.Element => {

    if (error) {
        console.log(error);
        return <div className="loader loader__error"><span>Sorry, something went wrong, please try again...</span></div>;
    }

    if (isLoading) {
        return <div className="loader loader__loading"><TbLoader3 /></div>;

    }

  return children;
};

export default Loader;

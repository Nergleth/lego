import { FigureType } from '../../api';
import {getRandom} from '../../utils';

export const transformFiguresResponse = (array: Array<FigureType>): Array<FigureType> => getRandom(array, 3);

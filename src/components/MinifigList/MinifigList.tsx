import React, { FC, useCallback, useMemo, useState } from 'react';
import classNames from 'classnames';

import { Loader, Figure } from '../';

import { FigureType, legoMinifiguresApi } from '../../api';
import { transformFiguresResponse } from './transformFiguresResponse';
import {useDispatchedActions} from '../../hooks';
import { minifiguresActions } from '../../store';

import './MinifigList.scss';

const MinifigList: FC = (): JSX.Element | null => {
    const [selectedFigure, setSelectedFigure] = useState<string>('');
    // @ts-ignore
    const { data: { results  = [] } = {} , error, isLoading} = legoMinifiguresApi.useGetFiguresQuery();
    const { chooseFigure, changeStep } = useDispatchedActions(minifiguresActions);
    const btnClassNames = classNames('minifig-list__btn', {
        'minifig-list__btn--disabled' : selectedFigure === ''
    });

    const figures = useMemo(() => transformFiguresResponse(results), [results]);

    const onFigureChosen = () => {
        chooseFigure(figures.find(figure => figure.set_num === selectedFigure) as FigureType);
        changeStep(3);
    }

    const onFigureSelect = useCallback((figId: string) => {
        setSelectedFigure(figId);
    }, []);

    const btnLabel = selectedFigure === '' ? 'Please, choose your figure first' : 'Proceed to shipment';

    return <Loader isLoading={isLoading} error={error}>
        <div className="minifig-list">
            <h2 className="minifig-list__header">Choose your minifig</h2>
            <div className="minifig-list__figures">
                {figures?.map(elem => <Figure key={elem.set_num} figure={elem} onFigureSelect={onFigureSelect} selected={ elem.set_num === selectedFigure } />)}
            </div>
            <button className={btnClassNames} onClick={onFigureChosen} disabled={selectedFigure === ''}>{btnLabel}</button>
        </div>
    </Loader>;
};

export default MinifigList;

import React, { FC, useState} from 'react';
import { useSelector } from 'react-redux';

import { FigureDetails, Loader, SubmitForm } from '../index';
import { legoMinifiguresApi } from '../../api';
import { selectFigure } from '../../store/minifigures';

import './FormScreen.scss';

const FormScreen: FC = (): JSX.Element | null => {
    const figure = useSelector(selectFigure);

    // @ts-ignore
    const { data: { results  = [] } = {} , error, isLoading} = legoMinifiguresApi.useGetFigurePartsQuery(figure?.set_num);

    if (!figure || !results) {
        return null;
    }

    return <div className="form-screen">
            <div className="form-screen__form-wrapper">
                <SubmitForm />
            </div>
            <div className="form-screen__summary">
                <div className="form-screen__flex-helper">
                    <div className="form-screen__summary-header">Summary</div>
                    <Loader isLoading={isLoading} error={error}>
                        <FigureDetails figure={figure} parts={results}/>
                    </Loader>
                </div>
                <button type="submit" form="purchase-form" className="form-screen__btn">Submit</button>
            </div>
    </div>;
};

export default FormScreen;

import { FigureType, PartType } from '../../api';

export type FigureDetailsPropsType = {
    figure: FigureType;
    parts: Array<PartType>;
    showMore?: boolean;
};

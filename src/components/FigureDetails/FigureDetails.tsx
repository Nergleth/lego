import React, { FC } from 'react';

import { FigureDetailsPropsType } from './types';

import './FigureDetails.scss';

const FigureDetails: FC<FigureDetailsPropsType> = ({ figure: { name, set_num, set_img_url, num_parts}, parts, showMore = false}): JSX.Element => {
  return <div className="figure-details">
    <div className="figure-details__img-wrapper">
      <img src={set_img_url} className="figure-details__img" alt={name} />
    </div>
    <div className="figure-details__name">{name}</div>
    <div className="figure-details__parts">There are {num_parts} parts in this minifig</div>
        <ul className="parts-list">
            {parts.map((result) =>
                <li key={result.set_num} className="part">
                  <img className="part__img" src={result.part.part_img_url} alt={result.part.name} />
                  <div className="part__wrapper">
                    <div className="part__name">{result.part.name}</div>
                    <div className="part__id">{result.part.part_num}</div>
                  </div>
                </li>
            )}
        </ul>
      {showMore && <a target="_blank" className="figure-details__more-link" href={`https://rebrickable.com/minifigs/${set_num}`} title="Show more details" rel="noreferrer">More details</a>}
  </div>;
};

export default FigureDetails;

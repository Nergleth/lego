import React, { useRef, useEffect, useState } from "react";
import { CgClose } from 'react-icons/cg';

import { ModalProps } from './types';

import './Modal.scss';

const Modal: React.FC<ModalProps> = ({isOpen, hasCloseBtn = true, onClose, children }) => {
    const [isModalOpen, setModalOpen] = useState(isOpen);
    const modalRef = useRef<HTMLDialogElement | null>(null);

    const handleCloseModal = () => {
        if (onClose) {
            onClose();
        }
        setModalOpen(false);
    };

    const handleKeyDown = (event: React.KeyboardEvent<HTMLDialogElement>) => {
        if (event.key === "Escape") {
            handleCloseModal();
        }
    };

    useEffect(() => {
        setModalOpen(isOpen);
    }, [isOpen]);

    useEffect(() => {
        const modalElement = modalRef.current;

        if (modalElement) {
            if (isModalOpen) {
                modalElement.showModal();
            } else {
                modalElement.close();
            }
        }
    }, [isModalOpen]);

    return (
        <dialog ref={modalRef} onKeyDown={handleKeyDown} className="modal">
            {hasCloseBtn && (
                <button className="modal__close-btn" onClick={handleCloseModal}>
                    <CgClose />
                </button>
            )}
            {children}
        </dialog>
    );
};

export default Modal;
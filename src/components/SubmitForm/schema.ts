import * as Yup from 'yup';

import { MESSAGES, Validators } from '../../utils';

export const initialFormValues = {
    email: '',
    name: '',
    surname: '',
    state: '',
    city: '',
    address: '',
    zipcode: '',
    phone: '',
    birthdate: '',
}

export const formSchema = Yup.object().shape({
    email: Yup.string()
        .email('Invalid email')
        .required(MESSAGES.REQUIRED),
    name: Validators.stringRequiredValidator,
    surname: Validators.stringRequiredValidator,
    state: Validators.stringRequiredValidator,
    city: Validators.stringRequiredValidator,
    address: Validators.stringRequiredValidator,
    zipcode: Validators.zipcodeValidator,
    phone: Validators.phoneValidator,
    birthdate: Validators.dateValidator,
})
export type FormDataType = {
    email: string;
    name: string;
    surname: string;
    state: string;
    city: string;
    address: string;
    zipcode: string;
    phone: string;
    birthdate: string;
}

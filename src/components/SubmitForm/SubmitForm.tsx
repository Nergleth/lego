import React, { FC } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import toast from 'react-simple-toasts';

import { formSchema, initialFormValues } from './schema';
import { useDispatchedActions } from '../../hooks';
import { minifiguresActions } from '../../store';
import { legoMinifiguresApi } from '../../api';
import { MESSAGES } from '../../utils';

import './SubmitForm.scss';

const SubmitForm: FC = () : JSX.Element => {
    const { changeStep } = useDispatchedActions(minifiguresActions);
    const [ postPurchaseMinifigure, { isSuccess, isError } ] = legoMinifiguresApi.usePostFigurePurchaseMutation();

    if ( isSuccess ) {
        toast(MESSAGES.PURCHASE_SUCCEEDED);
        changeStep(1);
    }

    if ( isError ) {
        toast(MESSAGES.PURCHASE_FAILED);
    }

    return <div className="submit-form">
      <h1 className="submit-form__header">Shipping details</h1>
      <Formik
          initialValues={initialFormValues}
          onSubmit={async (values) => {
              postPurchaseMinifigure(values);
              //changeStep(1);
          }}
          validationSchema={formSchema}
      >
        {({ isSubmitting }) => (
            <Form id="purchase-form">
                <div className="text-input-wrapper text-input-wrapper--half">
                    <label className="text-input-label" htmlFor="name">Name</label>
                    <Field className="text-input" type="text" name="name" />
                    <ErrorMessage className="text-input-error" name="name" component="div" />
                </div>
                <div className="text-input-wrapper text-input-wrapper--half">
                    <label className="text-input-label" htmlFor="name">Surname</label>
                    <Field className="text-input" type="text" name="surname" />
                    <ErrorMessage className="text-input-error" name="surname" component="div" />
                </div>
                <div className="text-input-wrapper">
                    <label className="text-input-label" htmlFor="phone">Phone Number</label>
                    <Field className="text-input" type="phone" name="phone" />
                    <ErrorMessage className="text-input-error" name="phone" component="div" />
                </div>
                <div className="text-input-wrapper">
                    <label className="text-input-label" htmlFor="email">Email</label>
                    <Field className="text-input" type="email" name="email" />
                    <ErrorMessage className="text-input-error" name="email" component="div" />
                </div>
                <div className="text-input-wrapper">
                    <label className="text-input-label" htmlFor="birthdate">Date of birth</label>
                    <Field className="text-input" type="date" name="birthdate" />
                    <ErrorMessage className="text-input-error" name="birthdate" component="div" />
                </div>
                <div className="text-input-wrapper">
                    <label className="text-input-label" htmlFor="address">Address</label>
                    <Field className="text-input" type="text" name="address" />
                    <ErrorMessage className="text-input-error" name="address" component="div" />
                </div>
                <div className="text-input-wrapper">
                    <label className="text-input-label" htmlFor="city">City</label>
                    <Field className="text-input" type="text" name="city" />
                    <ErrorMessage className="text-input-error" name="city" component="div" />
                </div>
                <div className="text-input-wrapper text-input-wrapper--half">
                    <label className="text-input-label" htmlFor="state">State</label>
                    <Field className="text-input" type="text" name="state" />
                    <ErrorMessage className="text-input-error" name="state" component="div" />
                </div>
                <div className="text-input-wrapper text-input-wrapper--half">
                    <label className="text-input-label" htmlFor="zipcode">Zip Code</label>
                    <Field className="text-input" type="string" name="zipcode" />
                    <ErrorMessage className="text-input-error" name="zipcode" component="div" />
                </div>
            </Form>
        )}
      </Formik>
  </div>;
};

export default SubmitForm;

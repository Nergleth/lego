import React, { FC } from 'react';

import { useDispatchedActions } from '../../hooks';
import { minifiguresActions } from '../../store';

import './ChooseBtn.scss';

type ChooseBtnProps = {};

const ChooseBtn: FC<ChooseBtnProps> = (): JSX.Element => {
    const { changeStep } = useDispatchedActions(minifiguresActions);

    const nextStep = () => {
        changeStep(2);
    }

    return <div className="choose-btn" onClick={nextStep}>Let's Go!</div>;
};

export default ChooseBtn;

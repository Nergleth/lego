import React, { FC } from 'react';

import { ChooseBtn } from '../index';

import './StartingScreen.scss';

const StartingScreen: FC = (): JSX.Element => {
  return <div className="starting-screen">
      <h1 className="starting-screen__header">Lego minifigs mystery box</h1>
      <ChooseBtn />
  </div>;
};

export default StartingScreen;

import React, {FC, useState} from 'react';
import classNames from 'classnames';

import { FigurePropsType } from './types';
import FigureDetailsModal from '../FigureDetailsModal';

import './Figure.scss';

const Figure: FC<FigurePropsType> = ({figure, onFigureSelect, selected}): JSX.Element => {
    const { set_num,name, set_img_url} = figure;
    const [isFigureDetailsModalOpen, setIsFigureDetailsModalOpen] = useState<boolean>(false);

    const figureClassNames = classNames('figure', {
      'figure__selected' : selected
    });

    const handleOpenFigureDetailsModal = () => {
        setIsFigureDetailsModalOpen(true);
    };

    const handleCloseFigureDetailsModal = () => {
        setIsFigureDetailsModalOpen(false);
    };

  const onShowDetailshandler = (e: React.MouseEvent<HTMLButtonElement>) => {
      e.stopPropagation();
      handleOpenFigureDetailsModal();
  }

  const onChoseFigure = () => {
    onFigureSelect(set_num);
  }

  return <>
      <div className={figureClassNames} onClick={onChoseFigure}>
          <div className="figure__img-wrapper">
              <img className="figure__img" src={set_img_url} alt={name} />
          </div>
          <p className="figure__name">{name}</p>
          <button className="figure__btn" onClick={onShowDetailshandler} >Show Details</button>
      </div>
      {isFigureDetailsModalOpen && <FigureDetailsModal isOpen={isFigureDetailsModalOpen} onClose={handleCloseFigureDetailsModal} figure={figure} />}
  </>;
};

export default Figure;

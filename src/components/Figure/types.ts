import { FigureType } from '../../api';

export type FigurePropsType = {
    figure: FigureType,
    onFigureSelect: (figId: string) => void,
    selected: boolean,
};

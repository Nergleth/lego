import { configureStore } from '@reduxjs/toolkit';

import minifiguresReducer from './minifigures/minifiguresSlice';
import { api } from '../api';

export const store = configureStore({
    reducer: {
        minifigures: minifiguresReducer,
        [api.reducerPath]: api.reducer,
    },
    middleware: (getDefaultMiddleware) => {
        const middleware = getDefaultMiddleware().concat(api.middleware);
        return middleware;
    },
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
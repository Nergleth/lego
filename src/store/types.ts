import { MinifiguresStateType } from './minifigures/types';

export interface AppStore {
    minifigures: MinifiguresStateType,
}

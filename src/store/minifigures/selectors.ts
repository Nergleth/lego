import { AppStore } from '../types';
import { FigureType } from '../../api';

export const selectStep = (state: AppStore): number => state.minifigures.step;
export const selectFigure = (state: AppStore): FigureType | null => state.minifigures.chosenFigure;

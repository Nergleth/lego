import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { MinifiguresStateType } from './types';
import {FigureType} from '../../api';

const initialState: MinifiguresStateType = {
    step: 1,
    chosenFigure: null,
}

export const minifiguresSlice = createSlice({
    name: 'minifigures',
    initialState,
    reducers: {
        changeStep: (state, action: PayloadAction<number>) => {
           state.step = action.payload;
        },
        chooseFigure: (state, action: PayloadAction<FigureType>) => {
            state.chosenFigure = action.payload;
        },
    },
})

// Action creators are generated for each case reducer function
export const minifiguresActions = minifiguresSlice.actions

export default minifiguresSlice.reducer
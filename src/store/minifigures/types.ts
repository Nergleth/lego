import { FigureType } from '../../api';

export interface MinifiguresStateType {
    step: number,
    chosenFigure: FigureType | null,
}

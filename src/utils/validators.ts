import * as Yup from "yup";
import parse from "date-fns/parse";

import { MESSAGES } from './messages';


// sample regex for phone number
export const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

export const zipcodeRegExp = /^\d{2}-\d{3}$/;

const stringRequiredValidator = Yup.string().required(MESSAGES.REQUIRED);

export const dateValidator = Yup.date()
    .transform(function (value, originalValue) {
        if (this.isType(value)) {
            return value;
        }
        return parse(originalValue, "dd/mm/yyyy", new Date());
    })
    .typeError(MESSAGES.INVALID_DATE)

export const phoneValidator = Yup.string()
    .required(MESSAGES.REQUIRED)
    .matches(phoneRegExp, MESSAGES.INVALID_PHONE)
    .min(9, MESSAGES.TOO_SHORT)
    .max(9, MESSAGES.TOO_LONG);

export const zipcodeValidator = Yup.string()
    .required(MESSAGES.REQUIRED)
    .matches(zipcodeRegExp, MESSAGES.INVALID_ZIPCODE)
    .min(6, MESSAGES.TOO_SHORT)
    .max(6, MESSAGES.TOO_LONG);

export const Validators = {
    dateValidator,
    stringRequiredValidator,
    phoneValidator,
    zipcodeValidator
}

export const MESSAGES = {
    REQUIRED: 'This field is required',
    INVALID_PHONE: 'Phone number must contain 9 digits',
    TOO_SHORT: 'Value is too short',
    TOO_LONG: 'Value is too long',
    INVALID_DATE: 'The date seems invalid',
    INVALID_ZIPCODE: 'The zipcode seems invalid',
    PURCHASE_SUCCEEDED: 'Your minifigure has been succesfully purchased!',
    PURCHASE_FAILED: 'Sorry, something went wrong!'
}

import { FetchBaseQueryArgs } from '@reduxjs/toolkit/dist/query/fetchBaseQuery';

/**
 * Includes `authorization` headers to the API requests
 */
const prepareAuthorizationHeaders: NonNullable<FetchBaseQueryArgs['prepareHeaders']> = (
  headers,
  { getState },
) => {
    headers.set('authorization', `key ${process.env.REACT_APP_REBRICKABLE_KEY}`);

  return headers;
};

export default prepareAuthorizationHeaders;

import {api, CollectionResponseType, FigureType, PartType} from './';
import Tags from './tags';
import { APP_MINIFIGURES_THEME } from '../constants';
import {FormDataType} from '../components/SubmitForm/types';



const legoMinifiguresApi = api.injectEndpoints({
  endpoints: (build) => ({
    getFigures: build.query<CollectionResponseType<FigureType>, void>({
      query: () => ({
        url: '/minifigs',
        params: {
          'in_theme_id' : APP_MINIFIGURES_THEME
        },
      }),
      providesTags: [Tags.MINIFIGURES],
    }),
    getFigureParts: build.query<CollectionResponseType<PartType>, void>({
      query: (figureId) => ({
        url: `/minifigs/${figureId}/parts`,
      }),
      providesTags: [Tags.MINIFIGURE],
    }),
    postFigurePurchase: build.mutation<{ status: string}, FormDataType>({
      query: (clientData ) => ({
        url: `http://localhost:3030/purchase`,
        method: 'POST',
        body: clientData,
      }),
      invalidatesTags: [Tags.PURCHASE],
    }),
  }),
});

export const {
  useGetFiguresQuery,
  useGetFigurePartsQuery,
  useLazyGetFigurePartsQuery,
  usePostFigurePurchaseMutation,
} = legoMinifiguresApi;

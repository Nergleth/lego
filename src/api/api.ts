import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import Tags from './tags';
import { prepareAuthorizationHeaders } from './helpers';

const baseQuery = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_URL,
  prepareHeaders: prepareAuthorizationHeaders,
});

const api = createApi({
  reducerPath: 'api',
  tagTypes: Object.values(Tags),
  baseQuery,
  endpoints: () => ({}),
});

export default api;

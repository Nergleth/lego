// eslint-disable-next-line no-shadow
enum Tags {
  MINIFIGURES = 'MINIFIGURES',
  MINIFIGURE = 'MINIFIGURE',
  PURCHASE = 'PURCHASE',
}

export default Tags;

export interface CollectionResponseType<T> {
    count: number;
    next: string | null;
    previous: string | null;
    results: Array<T>;
}

export interface FigureType {
    set_num: string;
    name: string;
    num_parts: number;
    set_img_url: string;
    set_url: string;
    last_modified_dt: string;
}

export interface PartType {
    id: number;
    inv_part_id: number;
    part: {
        part_num: number;
        name: string;
        part_cat_id: number;
        part_url: string;
        part_img_url: string;
        external_ids: Record<string, Array<string>>;
        print_of: string | null;
    };
    color: {
        id: number;
        name: string;
        rgb: string;
        is_trans: boolean;
        external_ids: Record<string, Record<'ext_ids' | 'ext_descrs', Array<number | null | string>>>;
    };
    set_num: string;
    quantity: number;
    is_spare: boolean;
    element_id: string;
    num_sets: number;
}

const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

server.use(jsonServer.bodyParser)
server.use(middlewares)

server.use((req, res, next) => {
    console.log("POST request listener");
    const body = req.body;
    console.log(body);
    if (req.method === "POST") {
        res.json({ message: "Your minifigure was succesfully purchased", status: "success"});
    }else{
        next();
    }
});

// Use default router
server.use(router)
server.listen(3030, () => {
    console.log('JSON Server is running')
})